function calc() {

    var convert = $("input[type='radio'][name='convert']:checked").val();

    console.log(convert);

    if (convert == 1) {
        let miles = parseInt(document.getElementById("miles").value);
        let gallons = parseInt(document.getElementById("gallons").value);
        let fahrenheit = parseInt(document.getElementById("fahrenheit").value);
        let feet = parseInt(document.getElementById("feet").value);

        let kilometer = convertMiles(miles);
        let liter = convertGallons(gallons);
        let celsius = convertFahrenheit(fahrenheit);
        let meter = convertFeet(feet);

        $("#kilometer").val(kilometer);
        $("#liter").val(liter);
        $("#celsius").val(celsius);
        $("#meter").val(meter);
    }

    if (convert == 2) {
        let kilometer = parseInt(document.getElementById("kilometer").value);
        let liter = parseInt(document.getElementById("liter").value);
        let celsius = parseInt(document.getElementById("celsius").value);
        let meter = parseInt(document.getElementById("meter").value);

        let miles = convertKilometer(kilometer);
        let gallons = convertLiter(liter);
        let fahrenheit = convertCelsius(celsius);
        let feet = convertMeter(meter);

        $("#miles").val(miles);
        $("#gallons").val(gallons);
        $("#fahrenheit").val(fahrenheit);
        $("#feet").val(feet);
    }
}

function convertMiles(m) {
    if (typeof m != 'number') {
        throw Error('The given argument is not a number');
    }
    return (parseInt(m) * 1.609344).toFixed(2);
}

function convertGallons(g) {
    if (typeof g != 'number') {
        throw Error('The given argument is not a number');
    }
    return (parseInt(g) * 3.785411784).toFixed(2);
}

function convertFahrenheit(f) {
    if (typeof f != 'number') {
        throw Error('The given argument is not a number');
    }
    return ((parseInt(f) - 32) / 1.8).toFixed(2);
}

function convertFeet(f) {
    if (typeof f != 'number') {
        throw Error('The given argument is not a number');
    }
    return (parseInt(f) * 0.3048).toFixed(2);
}

function convertKilometer(k) {
    if (typeof k != 'number') {
        throw Error('The given argument is not a number');
    }
    return (parseInt(k) / 1.609344).toFixed(2);
}

function convertLiter(l) {
    if (typeof l != 'number') {
        throw Error('The given argument is not a number');
    }
    return (parseInt(l) / 3.785411784).toFixed(2);
}

function convertCelsius(c) {
    if (typeof c != 'number') {
        throw Error('The given argument is not a number');
    }
    return ((parseInt(c) * 1.8)+32).toFixed(2);
}

function convertMeter(m) {
    if (typeof m != 'number') {
        throw Error('The given argument is not a number');
    }
    return (parseInt(m) / 0.3048).toFixed(2);
}

function clearAll() {
    console.log("clear");
    $(".form-control").val(parseInt(0).toFixed(2));
}