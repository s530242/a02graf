QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing convertMiles function with different inputs', function (assert) {
    assert.equal(convertMiles(1),1.61, 'Calculation Miles to Kilometer valid input');
    assert.equal(convertMiles(0),0, 'Calculation Miles to Kilometer input zero');
    assert.throws(function () {convertMiles(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertMiles("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');
});
QUnit.test('Testing convertGallons function', function (assert) {
    assert.equal(convertGallons(1),3.79, 'Calculation Gallons to Liter valid input');
    assert.equal(convertGallons(0),0, 'Calculation Gallons to Liter input zero');
    assert.throws(function () {convertGallons(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertGallons("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');
});
QUnit.test('Testing convertFahrenheit function', function (assert) {
    assert.equal(convertFahrenheit(80),26.67, 'Calculation Fahrenheit to Celsius valid input');
    assert.equal(convertFahrenheit(0),-17.78, 'Calculation Fahrenheit to Celsius input zero');
    assert.throws(function () {convertFahrenheit(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertFahrenheit("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');

});
QUnit.test('Testing convertFeet function', function (assert) {
    assert.equal(convertFeet(1),0.30, 'Calculation Feet to Meter valid input');
    assert.equal(convertFeet(0),0, 'Calculation Feet to Meter input zero');
    assert.throws(function () {convertFeet(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertFeet("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');

});
QUnit.test('Testing convertKilometer function', function (assert) {
    assert.equal(convertKilometer(1),0.62, 'Calculation Kilometer to Miles valid input');
    assert.equal(convertKilometer(0),0, 'Calculation Kilometer to Miles input zero');
    assert.throws(function () {convertKilometer(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertKilometer("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');

});
QUnit.test('Testing convertLiter function', function (assert) {
    assert.equal(convertLiter(10),2.64, 'Calculation Liter to Gallons valid input');
    assert.equal(convertLiter(0),0, 'Calculation Liter to Gallons input zero');
    assert.throws(function () {convertLiter(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertLiter("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');

});
QUnit.test('Testing convertCelsius function', function (assert) {
    assert.equal(convertCelsius(25),77.00, 'Calculation Celsius to Fahrenheit valid input');
    assert.equal(convertCelsius(0),32, 'Calculation Celsius to Fahrenheit input zero');
    assert.throws(function () {convertCelsius(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertCelsius("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');

});
QUnit.test('Testing convertMeter function', function (assert) {
    assert.equal(convertMeter(10),32.81, 'Calculation Meter to Feet valid input');
    assert.equal(convertMeter(0),0, 'Calculation Meter to Feet input zero');
    assert.throws(function () {convertMeter(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {convertMeter("Test"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');

});
